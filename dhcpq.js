const fs = require('fs');
const _ = require('lodash');
const dhcpdleases = require('dhcpd-leases');
const columnify = require('columnify');

const LEASES_FILE = '/usr/local/etc/dhcpd/dhcpd.leases'

var s = fs.readFileSync(LEASES_FILE, 'utf-8');

const toArrayWithKey = (obj, keyAs) => _.values(_.mapValues(obj, (value, key) => {
    value[keyAs] = key;
    return value;
}));

const ary = _.sortBy(toArrayWithKey(dhcpdleases(s), 'address'), 'address');

console.log(columnify(ary, {
    columns: ['address', 'client-hostname', 'hardware ethernet', 'binding state']
}));

