# README #

**Displays a formatted report from the information in the ISC DHCP leases file.**

Requires node.js.  The dhcpq file (without the .js) can be placed in /usr/local/bin and made executable.  It expects lodash, columnify and dhcpd-leases be installed globally (npm install -g <>).

### Configuration ###

* adjust gmodules variable in dhcpq file if your global modules are elsewhere.
* adjust leases_file varable to point at your leases file.

### Sample output ###

~~~~
$ dhcpq
ADDRESS         CLIENT-HOSTNAME            HARDWARE ETHERNET BINDING STATE
192.168.222.134                            10:5f:49:14:7b:3a active
192.168.222.135 apple                      8c:89:a5:16:15:25 active
192.168.222.136 Breakroom-PC2              b8:ae:ed:7c:40:06 active
192.168.222.137 peach                      c0:3f:d5:62:5d:23 active
192.168.222.138 pear                       10:c3:7b:4c:f5:3a active
192.168.222.139 plum                       e0:69:95:de:2c:5c active
192.168.222.143 DESKTOP-JPPD7OE            0c:54:a5:18:83:90 active
192.168.222.145 orange                     4c:72:b9:b1:22:4c active
192.168.222.146 lemon                      8c:89:a5:16:15:7b active
192.168.222.149 BSAP1925-00-19-92-33-0a-01 00:19:92:33:0a:01 active
192.168.222.150 BSAP1925-00-19-92-33-26-01 00:19:92:33:26:01 active
192.168.222.151 BSAP1925-00-19-92-33-16-02 00:19:92:33:16:02 active
192.168.222.152 BSAP1925-00-19-92-33-4c-02 00:19:92:33:4c:02 active
192.168.222.153 BSAP1925-00-19-92-33-45-02 00:19:92:33:45:02 active
192.168.222.154 BSAP1925-00-19-92-33-16-00 00:19:92:33:16:00 active
192.168.222.156 PRODUCTION_2NUC1           f4:4d:30:65:dc:00 active
192.168.222.171 lime                       2c:44:fd:12:2e:33 active
192.168.222.173 kiwi                       00:69:95:de:2c:54 active
192.168.222.174                            34:bd:fa:77:16:42 active
192.168.222.175 foo                        00:26:6c:23:c9:0c active
~~~~